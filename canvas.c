#include "xmplot.h"
#include "canvas.h"

#include <stdlib.h>
#include <math.h>

#define M_PI 3.141592653589793238462643383279502884L

viewport def_view = {-5,5,-2.0,2.0};
viewport view = {-5,5,-2.0,2.0};
int nsamp = 100;
function_cartesian f = {NULL,NULL,NULL,{1.0,1,0,0,LINE_SOLID}};
unsigned int precx,precy;

void pointer_accel(Bool flag) {
	static int num,denom,thresh;
	if(flag == True) {
		XChangePointerControl(fl_get_display(),True,True,num,denom,thresh);
	} else {
		XGetPointerControl(fl_get_display(),&num,&denom,&thresh);
		XChangePointerControl(fl_get_display(),True,True,0,1,0);
	}
	XFlush(fl_get_display());
}

void draw_crosshair(crosshair *c,FL_Coord x, FL_Coord y) {
	if(c->init == False) { c->init = True; }
	fl_winset(FL_ObjWin(canvas));
	fl_drawmode(GXinvert);
	fl_line(x,0,x,surfh,FL_WHITE);
	fl_line(0,y,surfw,y,FL_WHITE);
	fl_drawmode(GXcopy);
	c->x = x; c->y = y;
}

void reset_crosshair(crosshair *c) {
	c->x = -1; c->y = -1;
	c->init = False;
}

void draw_rubberband(rubberband *rb,FL_Coord x,FL_Coord y) {
	if(rb->init == False) {	rb->init = True; }
	fl_winset(FL_ObjWin(canvas));
	fl_drawmode(GXinvert);
	if(x >= rb->x0 && y >= rb->y0) {
		fl_rect(rb->x0,rb->y0,x-rb->x0,y-rb->y0,FL_WHITE);
		/*fl_point(rb->x00,rb->y0,FL_WHITE);*/
	} else if(x <= rb->x0 && y >= rb->y0) {
		fl_rect(x,rb->y0,rb->x0-x,y-rb->y0,FL_WHITE);
		/*fl_point(rb->x0,rb->y0,FL_WHITE);*/
	} else if(x >= rb->x0 && y <= rb->y0) {
		fl_rect(rb->x0,y,x-rb->x0,rb->y0-y,FL_WHITE);
		/*fl_point(rb->x0,rb->y0,FL_WHITE);*/
	} else {
		fl_rect(x,y,rb->x0-x,rb->y0-y,FL_WHITE);
		/*fl_point(rb->x0,rb->y0,FL_WHITE);*/
	}
	fl_drawmode(GXcopy);
	rb->x = x; rb->y = y;
}

void reset_rubberband(rubberband *rb) {
	rb->x0 = -1; rb->y0 = -1; rb->x = -1; rb->y = -1;
	rb->init = False;
}

void update_input_bounds() {
	char buf[7];
	snprintf(buf,7,"%%.%dLf",precx);
	fl_set_input_f(i_xl,buf,view.xl);
	fl_set_input_f(i_xh,buf,view.xh);
	snprintf(buf,7,"%%.%dLf",precy);
	fl_set_input_f(i_yl,buf,view.yl);
	fl_set_input_f(i_yh,buf,view.yh);
}

void update_zoom() {
	zoomx = surfw/XRANGE;
	zoomy = surfh/YRANGE;
	precx = (int)ceil(fabs(log10(1.0/zoomx)));
	precy = (int)ceil(fabs(log10(1.0/zoomy)));
}

#define STRIDE 1
void update_samp() {
	nsamp = surfw/STRIDE;
}

void update_position(double x,double y) {
	char buf[9];
	snprintf(buf,9,"X: %%.%df",precx);
	fl_set_object_label_f(t_xpos,buf,x);
	snprintf(buf,9,"Y: %%.%df",precy);
	fl_set_object_label_f(t_ypos,buf,y);
}

void clear_position() {
	fl_set_object_label_f(t_xpos,"X:");
	fl_set_object_label_f(t_ypos,"Y:");
}

double pix_to_view_x(FL_Coord pix) {
	return (double)(pix)/zoomx;
}

double pix_to_view_y(FL_Coord pix) {
	return (double)(pix)/zoomy;
}

Bool point_in_view(const viewport v,double x,double y) {
	if(x <= v.xh && x >= v.xl && y <= v.yh && y >= v.yl)
		return True;
	else
		return False;
}

/*int min_ix_in_view(const viewport v,const function_cartesian f) {*/
	/*return 0;*/
/*}*/

#define MIN(x,y) ((x)<(y)?x:y)
#define MAX(x,y) ((x)>(y)?x:y)
Bool point_in_box(const rubberband rb,double x,double y) {
	if(x <= view.xl+pix_to_view_x(MAX(rb.x0,rb.x)) && 
			x >= view.xl+pix_to_view_x(MIN(rb.x0,rb.x)) && 
			y <= view.yh-pix_to_view_y(MIN(rb.y0,rb.y)) && 
			y >= view.yh-pix_to_view_y(MAX(rb.y0,rb.y)))
		return True;
	else
		return False;
}
#undef MAX
#undef MIN

void redraw() {
	cairo_set_source_rgb(ctx,1,1,1);
	cairo_paint(ctx);
	draw_grid();
	draw_axes();
	draw_tics();
	if(f.f != NULL)
		draw_func(&f);
}

void draw_func(function_cartesian *f) {
	int i;
	double dx = XRANGE/nsamp;

	if(f->x != NULL)
		fl_free(f->x);
	if(f->y != NULL)
		fl_free(f->y);

	f->x = fl_malloc((nsamp+1) * sizeof(double));
	f->y = fl_malloc((nsamp+1) * sizeof(double));

	f->x[0] = view.xl;
	f->y[0] = evaluator_evaluate_x(f->f,f->x[0]);
	for(i=1;i<nsamp+1;i++) {
		f->x[i] = f->x[i-1] + dx;
		f->y[i] = evaluator_evaluate_x(f->f,f->x[i]);
		/*fprintf(stderr,"%f %f\n",f->x[i],f->y[i]);*/
	}

	cairo_set_antialias(ctx,antialias);
	cairo_set_source_rgb(ctx,f->ls.r,f->ls.g,f->ls.b);

	cairo_identity_matrix(ctx);
	cairo_translate(ctx,-(zoomx*view.xl),(zoomy*view.yh));
	
	cairo_set_line_width(ctx,f->ls.w);

	cairo_save(ctx);
	cairo_scale(ctx,zoomx,-zoomy);
	for(i=0;i<nsamp;i++) {
		if(isnan(f->y[i]) || isnan(f->y[i+1]))
			continue;
		if(point_in_view(view,f->x[i],f->y[i]) ||
				point_in_view(view,f->x[i+1],f->y[i+1]) ||
				(f->y[i] > view.yh && f->y[i+1] < view.yl) ||
				(f->y[i] < view.yl && f->y[i+1] > view.yh)) {
			cairo_move_to(ctx,f->x[i],f->y[i]);
			cairo_line_to(ctx,f->x[i+1],f->y[i+1]);
		}
	}
	cairo_restore(ctx);

	cairo_stroke(ctx);
}

#define NGRIDX 16
#define NGRIDY 12
void draw_grid() {
	long double dx,dy;
	long double x,y;

	dx = XRANGE/NGRIDX;
	dy = YRANGE/NGRIDY;

	cairo_set_source_rgb(ctx,0,0,0);

	cairo_identity_matrix(ctx);
	cairo_translate(ctx,-(zoomx*view.xl),(zoomy*view.yh));

	cairo_set_antialias(ctx,CAIRO_ANTIALIAS_GRAY);
	cairo_set_line_width(ctx,0.1);

	x = floorl(view.xl/dx) * dx;
	/*x = view.xl;*/
	while(x <= view.xh) {
		cairo_save(ctx);
		cairo_scale(ctx,zoomx,-zoomy);
		cairo_move_to(ctx,x,view.yl);
		cairo_line_to(ctx,x,view.yh);
		cairo_restore(ctx);
		cairo_stroke(ctx);
		x += dx;
	}
	y = floorl(view.yl/dy) * dy;
	/*y = view.yl;*/
	while(y <= view.yh) {
		cairo_save(ctx);
		cairo_scale(ctx,zoomx,-zoomy);
		cairo_move_to(ctx,view.xl,y);
		cairo_line_to(ctx,view.xh,y);
		cairo_restore(ctx);
		cairo_stroke(ctx);
		y += dy;
	}
}
#undef NGRIDY
#undef NGRIDX

void draw_axes() {
	long double x,y;

	x = view.xl>0?view.xl:(view.xh<0)?view.xh:0;
	y = view.yl>0?view.yl:(view.yh<0)?view.yh:0;

	cairo_set_source_rgb(ctx,0,0,0);
	cairo_identity_matrix(ctx);
	cairo_translate(ctx,-(zoomx*view.xl),(zoomy*view.yh));
	cairo_set_antialias(ctx,CAIRO_ANTIALIAS_GRAY);
	cairo_set_line_width(ctx,0.5);

	cairo_save(ctx);
	cairo_scale(ctx,zoomx,-zoomy);
	cairo_move_to(ctx,x,view.yl);
	cairo_line_to(ctx,x,view.yh);
	cairo_restore(ctx);
	cairo_stroke(ctx);

	cairo_save(ctx);
	cairo_scale(ctx,zoomx,-zoomy);
	cairo_move_to(ctx,view.xl,y);
	cairo_line_to(ctx,view.xh,y);
	cairo_restore(ctx);
	cairo_stroke(ctx);
}

#define NGRIDX 8
#define NGRIDY 12
void draw_tics() {
	long double dx, dy;
	long double x,y;
	long double bx,by;
	char fmt[8],ticbuf[16];
	cairo_text_extents_t ext;

	dx = XRANGE/NGRIDX;
	dy = YRANGE/NGRIDY;

	bx = view.xl>0?view.xl+1/zoomx:(view.xh<0)?view.xh-1/zoomx:0;
	by = view.yl>0?view.yl+1/zoomy:(view.yh<0)?view.yh-1/zoomy:0;

	cairo_set_source_rgb(ctx,0,0,0);

	cairo_identity_matrix(ctx);
	cairo_translate(ctx,-(zoomx*view.xl),(zoomy*view.yh));

	cairo_set_antialias(ctx,CAIRO_ANTIALIAS_GRAY);
	cairo_set_line_width(ctx,0.5);

	cairo_select_font_face(ctx,"sans-serif",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(ctx,11);

	snprintf(fmt,8,"%%+.%dLf",precx);
	x = floorl(view.xl/dx) * dx;
	/*x = view.xl;*/
	while(x <= view.xh) {
		cairo_save(ctx);
		cairo_scale(ctx,zoomx,-zoomy);
		cairo_move_to(ctx,x,by-3/zoomy);
		cairo_line_to(ctx,x,by+3/zoomy);
		cairo_restore(ctx);
		cairo_stroke(ctx);

		if(x < dx/2 && x > -dx/2) {
			x += dx;
			continue;
		}
		cairo_save(ctx);
		cairo_scale(ctx,1,1);
		snprintf(ticbuf,16,fmt,x);
		cairo_text_extents(ctx,ticbuf,&ext);
		cairo_move_to(ctx,x*zoomx-ext.width*0.7,-by*zoomy+((view.yl>0)?(ext.y_bearing+2):(-ext.y_bearing+4)));
		cairo_show_text(ctx,ticbuf);
		cairo_restore(ctx);
		x += dx;
	}
	snprintf(fmt,8,"%%+.%dLf",precy);
	y = floorl(view.yl/dy) * dy;
	/*y = view.yl;*/
	while(y <= view.yh) {
		cairo_save(ctx);
		cairo_scale(ctx,zoomx,-zoomy);
		cairo_move_to(ctx,bx-3/zoomx,y);
		cairo_line_to(ctx,bx+3/zoomx,y);
		cairo_restore(ctx);
		cairo_stroke(ctx);

		if(y < dy/2 && y > -dy/2) {
			y += dy;
			continue;
		}
		cairo_save(ctx);
		cairo_scale(ctx,1,-1);
		snprintf(ticbuf,16,fmt,y);
		cairo_text_extents(ctx,ticbuf,&ext);
		cairo_move_to(ctx,bx*zoomx+((view.xl>0)?(4):(-ext.width-6)),y*zoomy-ext.height/2);
		cairo_restore(ctx);
		cairo_show_text(ctx,ticbuf);
		y += dy;
	}
}
#undef NGRIDY
#undef NGRIDX
