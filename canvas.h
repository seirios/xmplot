#include <cairo.h>
#include <cairo-xlib.h>
#include <matheval.h>

#define XRANGE (view.xh-view.xl)
#define YRANGE (view.yh-view.yl)

typedef enum {
	XL,
	XH,
	YL,
	YH
} bound;

typedef enum {
	LINE_SOLID,
	LINE_DASH,
	LINE_DOT
} linetype;

typedef enum {
	FUNCTION_CARTESIAN,
	FUNCTION_POLAR,
	FUNCTION_PARAMETRIC,
	FUNCTION_IMPLICIT
} functype;

typedef struct {
	long double xl;
	long double xh;
	long double yl;
	long double yh;
} viewport;

typedef struct {
	Bool init;
	Bool show;
	FL_Coord x;
	FL_Coord y;
} crosshair;

typedef struct {
	Bool init;
	FL_Coord x0;
	FL_Coord y0;
	FL_Coord x;
	FL_Coord y;
} rubberband;

typedef struct {
	double w;
	double r;
	double g;
	double b;
	linetype lt;
} linestyle;

typedef struct {
	void *f;
	double *x;
	double *y;
	linestyle ls;
} function_cartesian;

typedef struct {
	void *f;
	double *r;
	double *t;
	linestyle ls;
} function_polar;

typedef struct {
	void *fx;
	void *fy;
	double *x;
	double *y;
	linestyle ls;
} function_parametric;

typedef struct {
	void *F;
	double *x;
	double *y;
	linestyle ls;
} function_implicit;

typedef union {
	function_cartesian car;
	function_polar pol;
	function_parametric par;
	function_implicit imp;
} function;
                               
typedef struct _flist {
	functype type;
	function f;
	struct _flist *next;
	struct _flist *prev;
} flist;

extern cairo_surface_t *surf;
extern cairo_t *ctx;
extern cairo_antialias_t antialias;

extern viewport def_view;
extern viewport view;
extern int surfw,surfh;
extern int nsamp;
extern double zoomx,zoomy;
extern FL_Coord gwx,gwy;
extern crosshair cursor;
extern rubberband zoombox;
extern function_cartesian f;
extern Bool moving,zooming;

extern unsigned int precx,precy;

int canvas_configure(FL_OBJECT*,Window,int,int,XEvent*,void*);
int canvas_expose(FL_OBJECT*,Window,int,int,XEvent*,void*);
int canvas_motion(FL_OBJECT*,Window,int,int,XEvent*,void*);
int canvas_button(FL_OBJECT*,Window,int,int,XEvent*,void*);
int canvas_hover(FL_OBJECT*,Window,int,int,XEvent*,void*);
int canvas_key(FL_OBJECT*,Window,int,int,XEvent*,void*);

void draw_crosshair(crosshair*,FL_Coord,FL_Coord);
void draw_rubberband(rubberband*,FL_Coord,FL_Coord);

void update_input_bounds();
void update_zoom();
void update_samp();
void update_position(double,double);

void clear_position();
void reset_crosshair(crosshair*);
void reset_rubberband(rubberband*);

double pix_to_view_x(FL_Coord);
double pix_to_view_y(FL_Coord);
Bool point_in_view(const viewport,double,double);
Bool point_in_box(const rubberband,double,double);

void redraw();
void draw_grid();
void draw_func(function_cartesian*);
void draw_axes();
void draw_tics();

void free_function_cartesian();

void pointer_accel(Bool);
