#include "xmplot.h"
#include "canvas.h"

#include <stdlib.h>
#include <errno.h>
#include <X11/keysym.h>

/* mainpanel */

void list_cb(FL_OBJECT * obj, long data) {}
void create_cb(FL_OBJECT * obj, long data) {}
void delete_cb(FL_OBJECT * obj, long data) {}
void show_cb(FL_OBJECT * obj, long data) {}
void browser_cb(FL_OBJECT * obj, long data) {}

int quit_cb(int data) {
	return FL_OK;
}

int cartesian_cb(int data) {
	if(fl_get_formbrowser_numforms(browser) == 1)
		fl_replace_formbrowser(browser,1,form_cartesian);
	else
		fl_addto_formbrowser(browser,form_cartesian);
	return FL_IGNORE;
}

int polar_cb(int data) {
	if(fl_get_formbrowser_numforms(browser) == 1)
		fl_replace_formbrowser(browser,1,form_polar);
	else
		fl_addto_formbrowser(browser,form_polar);
	return FL_IGNORE;
}

int parametric_cb(int data) {
	if(fl_get_formbrowser_numforms(browser) == 1)
		fl_replace_formbrowser(browser,1,form_parametric);
	else
		fl_addto_formbrowser(browser,form_parametric);
	return FL_IGNORE;
}

int implicit_cb(int data) {
	if(fl_get_formbrowser_numforms(browser) == 1)
		fl_replace_formbrowser(browser,1,form_implicit);
	else
		fl_addto_formbrowser(browser,form_implicit);
	return FL_IGNORE;
}


/* mainview */

crosshair cursor = {False,True,-1,-1};
rubberband zoombox = {False,-1,-1,-1,-1};
FL_Coord gwx,gwy;
Bool over = False;
Bool moving = False, zooming = False;

void input_func(FL_OBJECT * obj, long data FL_UNUSED_ARG) {
	f.f = evaluator_create((char*)fl_get_input(obj));
	if((void*)f.f != NULL) {
		fl_set_input_color(obj,FL_BLACK,FL_BLUE);
		redraw();
	} else
		fl_set_input_color(obj,FL_RED,FL_BLUE);
}

void input_cb(FL_OBJECT * obj, long data) {
	double val;
	errno = 0;
	val = strtod(fl_get_input(obj),NULL);
	if(data == XL) {
		if(errno != ERANGE && val < view.xh)
			view.xl = val;
	} else if(data == XH) {
		if(errno != ERANGE && val > view.xl)
			view.xh = val;
	} else if(data == YL) {
		if(errno != ERANGE && val < view.yh)
			view.yl = val;
	} else if(data == YH) {
		if(errno != ERANGE && val > view.yl)
			view.yh = val;
	}
	update_zoom();
	update_input_bounds();
	update_samp();
	redraw();
}

int canvas_configure(FL_OBJECT *obj FL_UNUSED_ARG, Window win FL_UNUSED_ARG,\
		int win_width FL_UNUSED_ARG, int win_height FL_UNUSED_ARG,\
		XEvent *xev, void *data FL_UNUSED_ARG) {
	int w,h;
	w = xev->xconfigure.width;
	h = xev->xconfigure.height;
	cairo_xlib_surface_set_size(surf,w,h);
	zoomx *= (double)w/surfw;
	zoomy *= (double)h/surfh;
	surfw = w; surfh = h;
	update_samp();
	redraw();
	reset_crosshair(&cursor);
	return FL_IGNORE;
}

int canvas_expose(FL_OBJECT *obj FL_UNUSED_ARG, Window win FL_UNUSED_ARG,\
		int win_width FL_UNUSED_ARG, int win_height FL_UNUSED_ARG,\
		XEvent *xev FL_UNUSED_ARG, void *data FL_UNUSED_ARG) {
	redraw();
	return FL_IGNORE;
}

int canvas_hover(FL_OBJECT *obj FL_UNUSED_ARG, Window win FL_UNUSED_ARG,\
		int win_width FL_UNUSED_ARG, int win_height FL_UNUSED_ARG,\
		XEvent *xev, void *data FL_UNUSED_ARG) {
	FL_Coord x,y;
	unsigned int kk;
	switch(xev->type) {
		case EnterNotify:
			fl_get_win_mouse(win,&x,&y,&kk);
			pointer_accel(False);
			if(moving == False && zooming == False) {
				fl_set_cursor(win,FL_INVISIBLE_CURSOR);
				draw_crosshair(&cursor,x,y);
			}
			over = True;
			update_position(view.xl+pix_to_view_x(x),view.yh-pix_to_view_y(y));
			break;
		case LeaveNotify:
			pointer_accel(True);
			fl_reset_cursor(win);
			if(moving == False && zooming == False)
				draw_crosshair(&cursor,cursor.x,cursor.y);
			over = False;
			clear_position();
			break;
	}
	return FL_IGNORE;
}

#define DELTA 30
int canvas_motion(FL_OBJECT *obj FL_UNUSED_ARG, Window win,\
		int win_width FL_UNUSED_ARG, int win_height FL_UNUSED_ARG,\
		XEvent *xev, void *data FL_UNUSED_ARG) {
	FL_Coord x,y;
	unsigned int kk;
	fl_get_win_mouse(win,&x,&y,&kk);
	if(xev->xmotion.state & Button1Mask && moving == True) {
		if(gwx-x >= -DELTA && gwx-x <= DELTA && gwy-y >= -DELTA && gwy-y <= DELTA) {
			view.xl += pix_to_view_x(gwx-x);
			view.xh += pix_to_view_x(gwx-x);
			view.yl -= pix_to_view_y(gwy-y);
			view.yh -= pix_to_view_y(gwy-y);
			update_input_bounds();
			redraw();
		}
		gwx = x; gwy = y;
		clear_position();
	} else if(xev->xmotion.state & Button3Mask && zooming == True) {
		if(zoombox.init == True)
			draw_rubberband(&zoombox,zoombox.x,zoombox.y); /* clear previous rubberband */
		draw_rubberband(&zoombox,x,y);
		update_position(view.xl+pix_to_view_x(x),view.yh-pix_to_view_y(y));
	} else if(over == True && cursor.show == True){
		if(cursor.init == True)
			draw_crosshair(&cursor,cursor.x,cursor.y); /* clear previous crosshair */
		draw_crosshair(&cursor,x,y);
		update_position(view.xl+pix_to_view_x(x),view.yh-pix_to_view_y(y));
	}
	return FL_IGNORE;
}
#undef DELTA

#define MIN(x,y) ((x)<(y)?x:y)
#define MAX(x,y) ((x)>(y)?x:y)
#define EPS 1e-8
int canvas_button(FL_OBJECT *obj FL_UNUSED_ARG, Window win,\
		int win_width FL_UNUSED_ARG, int win_height FL_UNUSED_ARG,\
		XEvent *xev, void *data FL_UNUSED_ARG) {
	/*int i;*/
	/*Bool in_box;*/
	static FL_Coord x1,y1,x2,y2;
	FL_Coord xmin,xmax,ymin,ymax;
	unsigned int kk;
	switch(xev->xbutton.button) {
		case 1:
			if(xev->xbutton.type == ButtonPress) {
				if(zooming == False) {
					moving = True;
					fl_get_win_mouse(win,&gwx,&gwy,&kk);
					fl_reset_cursor(win);
					antialias = CAIRO_ANTIALIAS_NONE;
					cursor.show = False;
				}
			} else {
				if(zooming == False) {
					moving = False;
					fl_get_win_mouse(win,&x1,&y1,&kk);
					update_position(view.xl+pix_to_view_x(x1),view.yh-pix_to_view_y(y1));
					fl_set_cursor(win,FL_INVISIBLE_CURSOR);
					antialias = CAIRO_ANTIALIAS_GOOD;
					redraw();
					cursor.show = True;
					draw_crosshair(&cursor,x1,y1);
				}
			}
			break;
		case 3:
			if(xev->xbutton.type == ButtonPress) {
				if(moving == False) {
					zooming = True;
					fl_get_win_mouse(win,&x1,&y1,&kk);
					zoombox.x0 = x1; zoombox.y0 = y1;
					fl_reset_cursor(win);
					cursor.show = False;
					draw_crosshair(&cursor,cursor.x,cursor.y);
				}
			} else {
				if(moving == False && zooming == True) {
					zooming = False;
					fl_get_win_mouse(win,&x2,&y2,&kk);
					xmin = MIN(x1,x2); xmax = MAX(x1,x2);
					ymin = MIN(y1,y2); ymax = MAX(y1,y2);
					/*i = 0; in_box = False;*/
					/*while(in_box == False && i<nsamp+1) {*/
						/*in_box = point_in_box(zoombox,f.x[i],f.y[i]);*/
						/*i++;*/
					/*}*/
					/* avoid zooming if box range too small */
					if(pix_to_view_y(ymax-ymin) > EPS && pix_to_view_x(xmax-xmin) > EPS) {
						if(shiftkey_down(xev->xbutton.state)) { /* zoom out */
							view.xh += (XRANGE - pix_to_view_x(xmax));
							view.xl -= pix_to_view_x(xmin);
							view.yl -= (YRANGE - pix_to_view_y(ymax));
							view.yh += pix_to_view_y(ymin);
						} else { /* zoom in */
							view.xh -= (XRANGE - pix_to_view_x(xmax));
							view.xl += pix_to_view_x(xmin);
							view.yl += (YRANGE - pix_to_view_y(ymax));
							view.yh -= pix_to_view_y(ymin);
						}
						update_zoom();
						update_input_bounds();
						update_samp();
					} else
						draw_rubberband(&zoombox,zoombox.x,zoombox.y);
					fl_get_win_mouse(win,&x1,&y1,&kk);
					update_position(view.xl+pix_to_view_x(x1),view.yh-pix_to_view_y(y1));
					fl_set_cursor(win,FL_INVISIBLE_CURSOR);
					redraw();
					cursor.show = True;
					draw_crosshair(&cursor,x1,y1);
				} else if(moving == False && zooming == False) {
					fl_get_win_mouse(win,&x1,&y1,&kk);
					update_position(view.xl+pix_to_view_x(x1),view.yh-pix_to_view_y(y1));
					fl_set_cursor(win,FL_INVISIBLE_CURSOR);
					redraw();
					cursor.show = True;
					draw_crosshair(&cursor,x1,y1);
				}
				reset_rubberband(&zoombox);
			}
			break;
	}
	return FL_IGNORE;
}
#undef EPS
#undef MAX
#undef MIN

#define SIGN(x) ((x)>=0?1.0:-1.0)
int canvas_key(FL_OBJECT *obj FL_UNUSED_ARG, Window win FL_UNUSED_ARG,\
		int win_width FL_UNUSED_ARG, int win_height FL_UNUSED_ARG,\
		XEvent *xev, void *data FL_UNUSED_ARG) {
	int i;
	int ixl=0,ixh=nsamp;
	switch(XLookupKeysym(&(xev->xkey),0)) {
		case XK_q:
			if(f.f != NULL)
				evaluator_destroy(f.f);
			fl_finish();
			break;
		case XK_0:
			view.xl = def_view.xl;
			view.xh = def_view.xh;
			view.yl = def_view.yl;
			view.yh = def_view.yh;
			update_zoom();
			update_input_bounds();
			clear_position();
			update_samp();
			redraw();
			reset_crosshair(&cursor);
			break;
		case XK_r:
			/* find root if only one on view */
			for(i=0;i<nsamp+1;i++) {
				if(point_in_view(view,f.x[i],f.y[i]) == True) {
					ixl = i;
					break;
				}
			}
			for(i=nsamp;i>=0;i--) {
				if(point_in_view(view,f.x[i],f.y[i]) == True) {
					ixh = i;
					break;
				}
			}
			fprintf(stderr,"%f %f\n",f.x[ixl],f.x[ixh]);
			if(SIGN(f.y[ixl]) == SIGN(f.y[ixh]))
				fprintf(stderr,"Same sign on both ends!\n");
			/*r = find_root_bisection(xl,xh);*/
			break;
		case XK_Escape:
			if(xev->xkey.state & Button3Mask) {
				draw_rubberband(&zoombox,zoombox.x,zoombox.y); /* clear rubberband */
				zooming = False;
			}
			break;
	}
	return FL_IGNORE;
}
#undef SIGN
