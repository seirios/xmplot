CC=c99 $(LDFLAGS)
#OPTIMFLAGS=-Os -march=native
LIBS=-lforms -lX11 -lcairo -lm -lXrender -lpixman-1 -lpthread -lXext -lmatheval -lgsl -lgslcblas
LDFLAGS=-Wl,--as-needed -Llib
WFLAGS=-Wall -Wextra -pedantic
INCLUDES=-Iinclude -Iinclude/cairo
#INCLUDES=-I/usr/include/cairo
CFLAGS=$(OPTIMFLAGS) $(WFLAGS) $(LIBS) $(INCLUDES)
DEPS = xmplot.h canvas.h Makefile
OBJ = xmplot.o xmplot_cb.o xmplot_main.o canvas.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

xmplot: $(OBJ)
	$(CC) $^ $(CFLAGS) -o xmplot
	strip xmplot

.PHONY: clean

clean:
	rm -f *.o *~ *.bak xmplot
