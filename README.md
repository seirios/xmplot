# xmplot
An improved kmplot clone without Qt/KDE.

Work in progess...

## Structure
+ Parser + Calculator (using a modified version of GNU libmatheval)
+ Mapper
+ Renderer (using cairo)
+ GUI (using xforms toolkit)

## Aims
+ Lots of functions! Both common and special
+ Multiple functions
+ Cartesian, polar, implicit and parametric functions
+ Root finding within viewport
+ Extrema finding within viewport
+ Derivatives
+ Area under curves within viewport
