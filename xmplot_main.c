#include "xmplot.h"
#include "canvas.h"

#include <gsl/gsl_errno.h>

cairo_t *ctx;
cairo_surface_t *surf;
int surfw,surfh;
double zoomx,zoomy;
cairo_antialias_t antialias = CAIRO_ANTIALIAS_GOOD;

int
main( int    argc,
      char * argv[ ] )
{
	FL_Coord x,y;
	FL_Coord w,h;
	int t,r,b,l;
	FL_IOPT fl_ctrl;

	gsl_set_error_handler_off();

	fl_ctrl.depth = 32;
	fl_ctrl.vclass = TrueColor;
	fl_ctrl.doubleBuffer = True;
	fl_set_defaults(FL_PDDepth|FL_PDVisual|FL_PDDouble,&fl_ctrl);

    fl_initialize( &argc, argv, "xmplot", 0, 0 );

    create_the_forms();

	/* mainview */

	fl_set_form_dblbuffer(mainview,1);

	fl_add_canvas_handler(canvas,ConfigureNotify,canvas_configure,NULL);
	fl_add_canvas_handler(canvas,Expose,canvas_expose,NULL);
	fl_add_canvas_handler(canvas,MotionNotify,canvas_motion,NULL);
	fl_add_canvas_handler(canvas,ButtonPress,canvas_button,NULL);
	fl_add_canvas_handler(canvas,ButtonRelease,canvas_button,NULL);
	fl_add_canvas_handler(canvas,EnterNotify,canvas_hover,NULL);
	fl_add_canvas_handler(canvas,LeaveNotify,canvas_hover,NULL);
	fl_add_canvas_handler(canvas,KeyPress,canvas_key,NULL);

	fl_set_input_return(i_xl,FL_RETURN_END_CHANGED);
	fl_set_input_return(i_xh,FL_RETURN_END_CHANGED);
	fl_set_input_return(i_yl,FL_RETURN_END_CHANGED);
	fl_set_input_return(i_yh,FL_RETURN_END_CHANGED);

	fl_show_form(mainview,FL_PLACE_CENTERFREE,FL_FULLBORDER,"xmplot :: View");

	fl_set_object_focus(mainview,i_func);

	fl_get_winsize(FL_ObjWin(canvas),&w,&h);
	surf = cairo_xlib_surface_create(fl_get_display(),FL_ObjWin(canvas),
			fl_state[fl_vmode].xvinfo->visual,w,h);
	surfw = w; surfh = h;
	cairo_xlib_surface_set_size(surf,surfw,surfh);
	ctx = cairo_create(surf);

	update_zoom();
	update_input_bounds();
	update_samp();

	/* mainpanel */

	fl_get_winorigin(mainview->window,&x,&y);
	fl_get_decoration_sizes(mainview,&t,&r,&b,&l);
	fl_set_form_position(mainpanel,x-l-330-r,y-t);

	fl_show_form(mainpanel,FL_PLACE_GEOMETRY,FL_TRANSIENT,"xmplot :: Panel");

    fl_do_forms();

	fl_finish();

    return 0;
}
